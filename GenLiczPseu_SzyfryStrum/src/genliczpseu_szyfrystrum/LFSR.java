/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genliczpseu_szyfrystrum;

import java.nio.ByteBuffer;
import java.util.BitSet;

/**
 *
 * @author piotr
 */
public class LFSR {
    
    BitSet initialBS;               // bitset początkowy
    BitSet returnBS;                // wynik zwrócony w postaci bitset'u
    BitSet wielomianBS;               // wskazuje bity poddawane op. XOR
    BitSet tempBS = new BitSet();
    BitSet tekstJawnyBS = new BitSet();
    BitSet kryptogramBS = new BitSet();
    int size;
    int returnSize;
    int cardinalityOfBitSet = 0;
    int stringLength = 0;
    String deText;

    public LFSR(String s) {
        wielomianBS = new BitSet();
        setWielomianBS(s);
        initialBS = new BitSet(size);
        returnBS = new BitSet();
        returnSize = 0;
        setInitialBS();
    }
    
    
    public void setInitialBS() {    // ustawienie początkowego bitset's
        initialBS.clear();
        initialBS.set(1);           // 0 1 1 0 gdzie indeksy 3 2 1 0
        initialBS.set(2);
        initialBS.set(5);
        initialBS.set(8);
        initialBS.set(10);
        initialBS.set(14);
        initialBS.set(21);
        System.out.println("\nInitialBS: " + initialBS);
    }
    
    /** @TODO: */ 
    
    
    public void printBitSet(BitSet bs, int size) {
        System.out.print("BS: ");
        for(int i = size-1; i>=0; i--) {
            if(bs.get(i)) System.out.print("1 ");
            else System.out.print("0 ");
        }
        //System.out.println("");
    }
    
    public String bitSetToString (BitSet bs, int size) {
        StringBuilder sb = new StringBuilder();
        sb.append("BS: ");
        for(int i = size-1; i>=0; i--) {
            if(bs.get(i)) sb.append("1 ");
            else sb.append("0 ");
        }
        //System.out.println("");
        return sb.toString();
    }
    
    public void setWielomianBS (String s) {
        wielomianBS.clear();
        String st = s.replaceAll("\\s", "");
        String stArray[] = st.split(",");
        int intArray[] = new int[stArray.length];
        
        System.out.print("Wielomian intArray: ");
        for (int i = 0; i < stArray.length; i++) {
            intArray[i] = Integer.parseInt(stArray[i]);
            System.out.print(intArray[i] + " ");
        }
        System.out.println("");
              
        for (int i = 0; i < intArray.length; i++) {
            wielomianBS.set(intArray[i]-1);
        }
        
        size = wielomianBS.length();
        
        printBitSet(wielomianBS, size);
    }
    
    public void nextReturnBit(boolean print) {
        
        // najmłodszy bit zostaje przepisany do wyniku
        returnBS.set(returnSize, initialBS.get(0));
        returnSize++;

        //obliczanie wyniku XOR
        tempBS = (BitSet) initialBS.clone();
        tempBS.and(wielomianBS);
        cardinalityOfBitSet = tempBS.cardinality();
        
        // przesunięcie bitów w prawo o jeden
        initialBS = initialBS.get(1, size);
        
        // wyniku XOR zapisany na najstarszym bicie
        if (cardinalityOfBitSet%2 == 1) initialBS.set(size-1, true);
        else initialBS.set(size-1, false);
        
        if(print) {
            System.out.print("InitialSB: ");
            printBitSet(initialBS, size);
            System.out.print("  ReturnBS: ");
            printBitSet(returnBS, returnSize);
            System.out.println("");
        }
        
    }
    
    public void convertStringToBitSet (String st) {
        // pobranie długości tekstu jawnego
        stringLength = st.length();
        // zdefiniowanie zestawu bitów o dł. 8bit * dł. tekstu
        BitSet bs = new BitSet(stringLength*8);
        // zdefiniowanie tablicy bajtów + zamiana string'a na tablicę bajtów
        byte myBytes[];
        myBytes = st.getBytes();
        
        // wydruk bajtów w linii
        System.out.println("Bytes - tekst jawny:");
        for(byte b: myBytes){
           System.out.print(b + " ");
        }
        System.out.println("\nBitSet - tekst jawny:");
        
        // zamiana bajtów na zestaw bitów 
        bs = bs.valueOf(ByteBuffer.wrap(myBytes));
        // wydruk bitów (wypisanie indeksów bitów o wartości 1)
        System.out.println(bs);
        tekstJawnyBS = (BitSet) bs.clone();
    }
    
    public void convertStringToBitSetDeszyfr (String st) {
        // pobranie długości tekstu jawnego
        stringLength = st.length();
        // zdefiniowanie zestawu bitów o dł. 8bit * dł. tekstu
        BitSet bs = new BitSet(stringLength*8);
        // zdefiniowanie tablicy bajtów + zamiana string'a na tablicę bajtów
        byte myBytes[];
        myBytes = st.getBytes();
        
        // wydruk bajtów w linii
        System.out.println("Bytes - tekst szyfru:");
        for(byte b: myBytes){
           System.out.print(b + " ");
        }
        System.out.println("\nBitSet - tekst szyfru:");
        
        // zamiana bajtów na zestaw bitów 
        bs = bs.valueOf(ByteBuffer.wrap(myBytes));
        // wydruk bitów (wypisanie indeksów bitów o wartości 1)
        System.out.println(bs);
        kryptogramBS = (BitSet) bs.clone();
    }
    
    public void szyfruj (String text) {
        returnBS.clear();
        returnSize = 0;
        setInitialBS();
        convertStringToBitSet(text);
        for(int i = 0; i<stringLength*8; i++) {
            nextReturnBit(false);
        }
        System.out.println("KLUCZ_bitSet:");
        printBitSet(returnBS, stringLength*8);
        System.out.println("\nTEKST JAWNY_bitSet");
        printBitSet(tekstJawnyBS, stringLength*8);
        System.out.println("\nKRYPTOGRAM_bitSet:");
        kryptogramBS = (BitSet) tekstJawnyBS.clone();
        kryptogramBS.xor(returnBS);
        printBitSet(kryptogramBS, stringLength*8);
        System.out.println("");
        
    }
    
    public void deszyfruj (String st) {
        convertStringBitsToBitSet(st);
        returnBS.clear();
        returnSize = 0;
        setInitialBS();
        for(int i = 0; i<stringLength*8; i++) {
            nextReturnBit(false);
        }
       
        System.out.println("\nKRYPTOGRAM_bitSet:");
        printBitSet(kryptogramBS, stringLength*8);
        
        System.out.println("\nKLUCZ_bitSet:");
        printBitSet(returnBS, stringLength*8);
        
        tekstJawnyBS = (BitSet) kryptogramBS.clone();
        tekstJawnyBS.xor(returnBS);
        System.out.println("\nTEKST JAWNY_bitSet:");
        printBitSet(tekstJawnyBS, stringLength*8);
        
        convertBitSetToString(tekstJawnyBS);
    }
    
    public void convertStringBitsToBitSet (String st) {
        int index = 0;
        kryptogramBS.clear();
        String s = st.replaceAll("\\s", "");
        if(s.length()%8 == 0) stringLength = s.length()/8;
        else stringLength = (s.length()/8) + 1;        
        for(index = 0; index < s.length(); index++) {
            if(s.charAt(s.length()-index-1) == '1') kryptogramBS.set(index);
        }
    }
    
    public void convertBitSetToString (BitSet bs) {
        byte myBytes[];
        myBytes = bs.toByteArray();
        // wydruk bajtów w linii
        System.out.println("\nBytes - tekst jawny:");
        for(byte b: myBytes){
           System.out.print(b + " ");
        }
    }
    
    public String convertBitSetToStringChar (BitSet bs) {
        byte myBytes[];
        myBytes = bs.toByteArray();
        String st = new String(myBytes);
        return st;
    }
}

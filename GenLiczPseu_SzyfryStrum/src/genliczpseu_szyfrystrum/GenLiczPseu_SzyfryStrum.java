/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genliczpseu_szyfrystrum;


import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.layout.StackPane;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author piotr
 */
public class GenLiczPseu_SzyfryStrum extends Application {
    
    private String mainTitle = "Bezpieczeństwo sieci komputerowych - zadanie 3";
    // main - główne okno do wyświetlania zawartości
    FlowPane main;
    
    public static String wielomian = "4,1";
    public static LFSR lfsr = new LFSR(wielomian);
    private Timeline timeline;
    private boolean action = true;
    
    @Override
    public void start(Stage primaryStage) {
        // inicjalizacja menu aplikacji
        MenuBar menubar = new MenuBar();
        Menu menu1 = new Menu("Menu");
        MenuItem menuitem1 = new MenuItem("Generator liczb pseudolosowych");
        // akcja po wybraniu elementu 1 menu
        menuitem1.setOnAction(e -> {
            primaryStage.setTitle(mainTitle + " - LFSR");
            if (!main.getChildren().isEmpty()) {
                main.getChildren().clear();
            }
            // tworzenie layoutu do zadania
            LayoutGenerator();
        });
        MenuItem menuitem2 = new MenuItem("Szyfr strumieniowy");
        // akcja po wybraniu elementu 2 menu
        menuitem2.setOnAction(e -> {
            primaryStage.setTitle(mainTitle + " - Szyfr strumieniowy");
            // czyszczenie okna
            if (!main.getChildren().isEmpty()) {
                main.getChildren().clear();
            }
            // tworzenie layoutu do zadania
            LayoutSzyfrStrumieniowy();
        });
        
        MenuItem menuitem3 = new MenuItem("Ustawienia");
        // akcja po wybraniu elementu 3 menu
        menuitem3.setOnAction(e -> {
            primaryStage.setTitle(mainTitle + " - Ustawienia");
            // czyszczenie okna
            if (!main.getChildren().isEmpty()) {
                main.getChildren().clear();
            }
            // tworzenie layoutu do zadania
            LayoutUstawienia();
        });                      
        
        menu1.getItems().add(menuitem1);
        menu1.getItems().add(menuitem2);
        menu1.getItems().add(menuitem3);
        menubar.getMenus().add(menu1);
                // główny layout aplikacji z implementacją menu
        VBox root = new VBox(menubar);

        // main - główne okno do wyświetlania zawartości
        main = new FlowPane();
        main.setPadding(new Insets(30, 30, 30, 30));
        root.getChildren().add(main);

        // tworzenie okna aplikacji
        Scene scene = new Scene(root, 600, 400);
        primaryStage.setTitle(mainTitle);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        System.out.println(lfsr.initialBS.length());
        lfsr.printBitSet(lfsr.initialBS, lfsr.size);
        System.out.println("");
        
        launch(args);
    }
       
    public void LayoutGenerator() {
        
        Label textLabel = new Label("Generator liczb pseudolosowych.\nZmiana wielomianu możliwa w ustawieniach.");
        Label returnLabel = new Label("Wynik:");
        Label returnText = new Label(" - pusty - ");
        HBox keyBox = new HBox();
        
        timeline = new Timeline(new KeyFrame(Duration.millis(1000), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                lfsr.nextReturnBit(true);
                //lfsr.printBitSet(lfsr.returnBS, lfsr.returnSize);
                returnText.setText(lfsr.bitSetToString(lfsr.returnBS, lfsr.returnSize));
            }
	}));
        timeline.setCycleCount(Timeline.INDEFINITE);
	timeline.setAutoReverse(false);
        
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        Button button = new Button("Start");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	if(action) {
                    timeline.play();
                    action = false;
            		button.setText("Stop");
            	} else {
            		timeline.pause();
            		action = true;
            		button.setText("Start");
            	}
            }
        });
        
        Button resetButton = new Button("Reset");
        resetButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
            	lfsr.returnBS.clear();
                lfsr.returnSize = 0;
                lfsr.setInitialBS();
                returnText.setText("");
            }
        });
                
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(button);
        keyBox.getChildren().add(resetButton);
                
        main.setOrientation(Orientation.VERTICAL);
        main.setVgap(15);
        main.getChildren().add(textLabel);
        main.getChildren().add(returnLabel);
        main.getChildren().add(returnText);
        main.getChildren().add(keyBox);  
    }
    
    public void LayoutSzyfrStrumieniowy () {
        Label plainTextLabel = new Label("Zmiana wielomianu możliwa w ustawieniach.\nTekst do zaszyfrowania / odszyfrowania:");
        Label text = new Label("Wynik (tekst / postać binarna):");
        Label stringText = new Label(" - pusty - ");
        TextField szyfrogramText = new TextField(" - pusty - ");
        TextField plainTextField = new TextField();
        plainTextField.setPadding(new Insets(15, 15, 15, 15));
        HBox keyBox = new HBox();
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        Button szyfruj = new Button("Szyfrowanie");
        szyfruj.setOnAction(c -> {
            String plainText = plainTextField.getText();
            lfsr.szyfruj(plainText);
            stringText.setText(lfsr.convertBitSetToStringChar(lfsr.kryptogramBS));
            szyfrogramText.setText(lfsr.bitSetToString(lfsr.kryptogramBS, lfsr.returnSize));
        });
        Button deszyfruj = new Button("Odszyfrowywanie");
        deszyfruj.setOnAction(c -> {
            String plainText = plainTextField.getText();
            lfsr.deszyfruj(plainText);
            stringText.setText(lfsr.convertBitSetToStringChar(lfsr.tekstJawnyBS));
            szyfrogramText.setText(lfsr.bitSetToString(lfsr.tekstJawnyBS, lfsr.returnSize));
        });
        
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(szyfruj);
        keyBox.getChildren().add(deszyfruj);
        
        main.setOrientation(Orientation.VERTICAL);
        main.setVgap(15);
        main.getChildren().add(plainTextLabel);
        main.getChildren().add(plainTextField);
        main.getChildren().add(stringText);
        main.getChildren().add(szyfrogramText);
        main.getChildren().add(keyBox);    
    }
    
    public void LayoutUstawienia() {
        Label settingsLabel = new Label("Ustawiewnia wielomianu w formacie x, x, ...");
        Label messageText = new Label("");
        TextField settingsTextField = new TextField(wielomian);
        settingsTextField.setPadding(new Insets(15, 15, 15, 15));
        HBox keyBox = new HBox();
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        Button saveButton = new Button("Zapisz");
        saveButton.setOnAction(c -> {
            String settingsText = settingsTextField.getText();
            wielomian = settingsText;
            lfsr.setWielomianBS(wielomian);
            messageText.setText("Wielomian zapisany pomyślnie!");
            
        });
        
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(saveButton);
        
        main.setOrientation(Orientation.VERTICAL);
        main.setVgap(15);
        main.getChildren().add(settingsLabel);
        main.getChildren().add(settingsTextField);
        main.getChildren().add(keyBox);
        main.getChildren().add(messageText);
    
    }
    
}
